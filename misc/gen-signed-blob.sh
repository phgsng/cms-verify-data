#!/usr/bin/env bash
set -euo pipefail

declare -r keyname=key
declare -r data=data

# create rsa key and self-signed cert
openssl req \
    -nodes -newkey rsa:2048 \
    -x509 -days 36500 --outform PEM \
    -subj "/C=EU/CN=root-ca.starfleet.gov/O=Starfleet Headquarters" \
    -addext "subjectAltName = DNS:root-ca.starfleet.gov, email:ca@starfleet.gov" \
    -addext "keyUsage = critical, keyCertSign, digitalSignature" \
    -addext "basicConstraints = CA:TRUE" \
    -text \
    -keyout "${keyname}.rsa.pem" \
    -out "${keyname}.cert.pem"

# create raw data to sign and hash it
printf "hello, world!" >"${data}.txt"
openssl dgst -sha256 -binary <"${data}.txt" >"${data}.sha256"

# create CMS container with the data blob signed by the key
openssl cms -sign \
    -outform PEM \
    -md sha256 \
    -in "${data}.txt" \
    -binary \
    -nodetach \
    -inkey "${keyname}.rsa.pem" \
    -signer "${keyname}.cert.pem" \
    >"${data}.cms.pem"

# dump CMS as text
openssl cms -print -inform PEM -cmsout <"${data}.cms.pem" >"${data}.cms.pem.txt"

# verify CMS with the created key
openssl cms -verify -binary -inform PEM -CAfile "${keyname}.cert.pem" <"${data}.cms.pem"
