cms-blob   = data.cms.pem
cms-script = misc/gen-signed-blob.sh
cms-clutter= $(cms-blob) data.cms.pem.txt data.sha256 data.txt key.cert.pem key.rsa.pem
src        = src/main.rs Cargo.toml Cargo.lock
bin 	   = target/debug/cms-verify-data

demo: $(cms-blob) $(bin)
	$(bin) <$(cms-blob)

$(cms-blob): $(cms-script)
	$(cms-script)

$(bin): $(src)
	cargo build

clean:
	rm -r -- target
	rm -f -- $(cms-clutter)

.PHONY: clean
