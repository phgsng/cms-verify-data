#![allow(unused)]
use const_oid::{db as oiddb, AssociatedOid};
use der::{asn1::{ObjectIdentifier, OctetStringRef, SequenceRef},
          referenced::{OwnedToRef, RefToOwned},
          Decode, DecodePem, Encode, SliceWriter};
use pkcs7::{algorithm_identifier_types::{DigestAlgorithmIdentifier,
                                         DigestAlgorithmIdentifiers},
            certificate_choices::CertificateChoices,
            cms_version::CmsVersion,
            encapsulated_content_info::EncapsulatedContentInfo,
            encrypted_data_content::EncryptedDataContent,
            enveloped_data_content::EncryptedContentInfo,
            signed_data_content::{CertificateSet, SignedDataContent},
            signer_info::{SignerInfo, SignerInfos},
            ContentInfo, ContentType};
use rsa::{pkcs1v15::{Pkcs1v15Sign, Signature, SigningKey, VerifyingKey},
          pkcs8::DecodePublicKey,
          signature::DigestVerifier,
          RsaPublicKey};
use sha2::{Digest, Sha256};
use spki::{AlgorithmIdentifierRef, SubjectPublicKeyInfo};
use x509_cert::certificate::Certificate;

fn main()
{
    let raw = {
        use std::io::Read;
        let mut pem = Vec::new();
        std::io::stdin().read_to_end(&mut pem).unwrap();
        let (label, raw) = pem_rfc7468::decode_vec(&pem).unwrap();
        assert_eq!(label, "CMS");
        raw
    };
    let SignedDataContent {
        version,
        digest_algorithms,
        encap_content_info,
        certificates,
        crls,
        signer_infos,
    } = if let ContentInfo::SignedData(signed) =
        ContentInfo::from_der(&raw).unwrap()
    {
        signed
    } else {
        panic!("nope");
    };

    let e_content = encap_content_info.e_content.as_ref().unwrap();
    let signer = certificates.as_ref().unwrap().iter().next().unwrap();
    let spki = if let CertificateChoices::Certificate(Certificate {
        tbs_certificate,
        ..
    }) = signer
    {
        tbs_certificate.subject_public_key_info.owned_to_ref()
    } else {
        panic!("signer pubkey");
    };
    let pubkey = RsaPublicKey::try_from(spki).unwrap();
    let signer_info = signer_infos.iter().next().unwrap();
    assert_eq!(
        signer_info.signature_algorithm.oid,
        oiddb::rfc5912::RSA_ENCRYPTION
    );
    assert_eq!(signer_info.digest_algorithm.oid, oiddb::rfc5912::ID_SHA_256);

    eprintln!(
        "e_content digest: {:x?}",
        Sha256::digest(
            &e_content.decode_as::<OctetStringRef>().unwrap().as_bytes()
        )
    );

    let attrs =
        signer_info.signed_attributes.as_ref().unwrap().to_der().unwrap();
    assert!(attrs[0] == 0x31);

    let res1 = verify_1(
        &pubkey,
        &e_content.decode_as::<OctetStringRef>().unwrap().as_bytes(),
        &attrs,
        &signer_info.signature.as_bytes(),
    );

    let res2 = verify_2(
        &pubkey,
        &e_content.decode_as::<OctetStringRef>().unwrap().as_bytes(),
        &attrs,
        &signer_info.signature.as_bytes(),
    );

    if res1 {
        println!("res1 good");
    }

    if res2 {
        println!("res2 good");
    }

    assert!(res1 || res2);
}

fn verify_1(
    pubkey: &RsaPublicKey,
    e_content: &[u8],
    attrs: &[u8],
    signature: &[u8],
) -> bool
{
    let mut hasher = Sha256::new();
    hasher.update(&attrs);
    pubkey
        .verify(
            Pkcs1v15Sign::new::<Sha256>(),
            //Pkcs1v15Sign::new_unprefixed(),
            hasher.finalize().as_slice(),
            signature,
        )
        .is_ok()
}

fn verify_2(
    pubkey: &RsaPublicKey,
    e_content: &[u8],
    attrs: &[u8],
    signature: &[u8],
) -> bool
{
    let digest = {
        let mut hasher = Sha256::new();
        hasher.update(&attrs);
        hasher
    };

    VerifyingKey::<Sha256>::new(pubkey.clone())
        .verify_digest(digest, &Signature::try_from(signature).unwrap())
        .is_ok()
}
